+++
title = "Outreach Event Rally For Jesus"
date = "2022-07-01"
category = "resources"
+++

Purpose: Get People To Jesus And Jesus To People  

#   
**Saturday August 27th 2022**  

Setup: 10AM, Event: Noon to 3PM  
at Calvary Chapel Kaneohe Hawaii 

|  |  |
|--|--| 
| Address | 47-525 Kamehameha HWY. Kaneohe, HI 96744 | 
| **Rally&#x202F;Contact** | HandsForHisHarvest@gmail.com (Tanja NICKOLIC) |
| Rally&#x202F;Details | Share the Good News and bring together brothers and sisters from all over the island. Please invite your family and friends, create or buy signs/banners/flags, bring shofars... :trumpet: :partying_face: The organizers are encouraging everyone to PRAY for the event, for God to send people with open hearts, for good weather, protection and that Jesus be glorified in every aspect of the event :pray: | 
| Rally&#x202F;Support | Calvary Chapel Kaneohe and Pastor JD FARAG | 
| Website | https://www.jdfarag.org |
| Email | Office@CalvaryChapelKaneohe.com |
| Phone | +1 808-262-8800 |
|  |  |

Please continue your browsing on [jdfarag.org](https://www.jdfarag.org) for sermons, prophecy updates, ABC of salvation, Community forum 
and everything related to Pastor JD and Calvary Chapel Kaneohe. This page was just for the special event.  

#  
** All The Glory To God :heart:  

