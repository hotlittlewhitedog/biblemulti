+++
title = "Como ser salvo?"
date = "2022-08-10"
category = "resources"
+++

{{< youtube cUJJA1z4gOI >}}

#
# O apóstolo Paulo nos diz o que é o evangelho

> **1Coríntios 15:1-4**: TAMBÉM vos notifico, irmãos, o evangelho que já vos tenho anunciado; o qual também recebestes, e no qual também permaneceis. Pelo qual também sois salvos se o retiverdes tal como vo-lo tenho anunciado; se não é que crestes em vão. Porque primeiramente vos entreguei o que também recebi: que Cristo morreu por nossos pecados, segundo as Escrituras, E que foi sepultado, e que ressuscitou ao terceiro dia, segundo as Escrituras.

#
# Simples como um ABC
  
**A) ADMITE QUE ÉS PECADOR(A) E QUE PRECISAS DE UM SALVADOR.**
  
  É aqui que essa tristeza segundo Deus leva a um verdadeiro arrependimento por ter pecado contra o Deus justo e há uma mudança de coração, mudamos de ideia e Deus muda nossos corações e nos regenera por dentro.  

---
  
**B) ACREDITA QUE JESUS É O SENHOR E CREIA EM SEU CORAÇÃO QUE JESUS CRISTO MORREU POR SEUS PECADOS, FOI ENTERRADO, E QUE DEUS RESSUSCITOU JESUS DENTRE OS MORTOS.**
  
  É crer com todo o seu coração que Jesus Cristo é o que ele disse que era e que Ele morreu por nós por nossos pecados.  

---
  
**C) CLAMA PELO SENHOR.**
  
  É crer com todo o seu coração que Jesus Cristo é o que ele disse que era. Cada pessoa que viveu desde Adão dobrará o joelho e confessará com a boca que Jesus Cristo é o Senhor, o Senhor dos Senhores e o Rei dos Reis.  
  

