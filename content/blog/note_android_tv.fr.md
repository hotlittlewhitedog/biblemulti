+++
title = "Note to Android TV users"
date = "2024-09-09"
category = "resources"
keywords = "['biblemulti.org', 'bible android tv', 'bible androidtv', 'android sideload', 'bible multi', bible kjv', 'kjv', 'bible app', 'bible apps', 'bible android', 'bible apk', 'bible ios', 'bible mac', 'bible linux', 'bible terminal', 'bible flatpak', 'bible snap', 'bible christian', 'christian', bible multi the light', 'biblemultithelight', bible multi the life', 'biblemultithelife', 'bible multi the son of man', 'biblemultithesonofman', 'sonofman', 'god', 'jesus', 'the life', 'the light', 'free', 'hlwd', 'torrent', 'evangelism', 'apocalypse', 'revelation', 'alternate backup', 'bible offline']" 
+++
 
These actions are only necessary if The Light is not on Android TV store or not updated.  

Please read until the end, it's an easy solution in 4 steps :)

--- 

Since version ~3.82, The Light has been again rejected from Android TV!  
Always the same story, I'm tired of them seriously with all their updates, split of versions and removing functionnalities: "Not compliant to the new guidelines, blah, blah". I was not able to solve the problem of Audio which works perfectly but not integrated at 100% to their mediastore!   

--- 

OK, we have a solution, it's easy, it's __sideload__!  


## 1) You need a good browser:
Install TV BRO (if you don't have a browser working for that purpose), it's free.  


## 2) You need a sideload tool:
Install X-Plore, it's free.  


## 3) You need to download the Apk of The Light:

- If you use The Light from F-Droid.org or you are new to The Light,  
[goto F-Droid.org](https://f-droid.org/en/packages/org.hlwd.bible/) and download the latest version of The Light if you can reach them.  
Rem: the Apk on F-Droid has been resigned by F-Droid.  
If you can't reach them, [goto this page](/biblemulti/blog/torrent/) and download a version signed by F-Droid.

- If you use The Light from Google or Gitlab in the past,  
[goto this page](/biblemulti/blog/torrent/) and download the latest version of The Light signed by hotlittlewhitedog.  
Rem: because the Apk on Google and Gitlab have the signature of hotlittlewhitedog.

- If you use The Light from another repo, use that repo or the app could be reinstalled.  


## 4) Let's install the file downloaded:
Run X-Plore.  
Locate the file downloaded (probably in /storage/emulated/0/Download).  
Long click the file => Rename to thelight.__apk__ (if it's thelight.zip)  
Long click thelight.apk => Install.  

Enjoy!
