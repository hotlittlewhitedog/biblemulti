---
title: "Community"
date: 2022-07-15T15:58:41+02:00
category: "resources"
---

- [Forum of Pastor JD Farag](https://forum.jdfarag.org)

- [Website of Pastor JD Farag](https://www.jdfarag.org)

- [My Twitter](https://twitter.com/_hlwd)

- [My Telegram](https://t.me/biblemultithelight)
