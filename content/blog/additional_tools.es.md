+++
title = "Additional Tools"
date = "2022-07-26"
category = "resources"
+++

Be vigilant, always check in the Bible.  

Following apps work offline, I often use them.  
  
---
# 
# (Multi) GotQuestions
They have several applications for Android, iPhone, Mac...  
Rem: They support multi languages in app; there are more topics if you select English

- [Website](https://www.gotquestions.org)
- [Apps](https://www.gotquestions.org/apps.html)  
  
---
# 
# (French) Bibliquest 
Rem: Seems only in French :pensive:

- [Website](https://www.bibliquest.net)
- [Google](https://play.google.com/store/apps/details?id=com.bibliquest.app)
